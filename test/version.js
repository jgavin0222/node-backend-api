var app = require('../app/index.js');
var chai = require('chai');
var request = require('supertest');
var assert = require('assert');
var should = require('should');

var url = '/';

describe('GET '+url, ()=>{
  it('responds with correct data', (done)=>{
    request(app).
      get(url).
      send().
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);
        res.body.status.should.equal('success');
        res.body.version.should.equal(process.env.npm_package_version);

        return done();
    });
  });
});
