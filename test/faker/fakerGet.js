var app = require('../../app/index.js');
var chai = require('chai');
var request = require('supertest');
var assert = require('assert');
var should = require('should');

var url = '/faker';

describe('GET '+url, ()=>{
  it('responds with a faker string', (done)=>{
    request(app).
      get(url).
      send().
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);
        res.body.status.should.equal('success');
        (typeof res.body.fake).should.equal('string');

        return done();
    });
  });
});
