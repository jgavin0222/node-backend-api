var app = require('../../app/index.js');
var chai = require('chai');
var request = require('supertest');
var assert = require('assert');
var should = require('should');

var url = '/faker';
var username = 'username';
var password = 'password';

describe('POST '+url, ()=>{
  it('logs in successfully', (done)=>{
    var data = {
      username,
      password
    };
    request(app).
      post(url).
      send(data).
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(201);
        res.body.status.should.equal('success');
        res.body.username.should.equal(username);

        return done();
    });
  });
  it('fails when username is missing', (done)=>{
    request(app).
      post(url).
      send({password}).
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(401);
        res.body.status.should.equal('error');
        res.body.error.should.equal('missing parameter(s)');

        return done();
    });
  });
  it('fails when password is missing', (done)=>{
    request(app).
      post(url).
      send({username}).
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(401);
        res.body.status.should.equal('error');
        res.body.error.should.equal('missing parameter(s)');

        return done();
    });
  });
  it('fails when both username and password are missing', (done)=>{
    request(app).
      post(url).
      send().
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(401);
        res.body.status.should.equal('error');
        res.body.error.should.equal('missing parameter(s)');

        return done();
    });
  });
});
