const express = require('express');
const app = express();
const port = 4500;
const bodyParser = require('body-parser');
const faker = require('faker');

app.use(bodyParser.json());

app.get('/', (req, res)=>{
	var json = {
		status: 'success',
		name: 'Backend API Server',
		version: process.env.npm_package_version
	};
	res.status(200).send(json);
});

app.get('/faker', (req, res)=>{
	var json = {
		status: 'success',
		fake: faker.fake('{{random.word}}')
	};
	res.status(200).send(json);
});

app.post('/faker', (req, res)=>{
	var {username, password} = req.body;
	var errorJson = {
		status: 'error',
		error: 'missing parameter(s)'
	};
	var json = {
		status: 'success',
		username
	};
	if(typeof username === 'undefined' || typeof password === 'undefined'){
		res.status(401).send(errorJson);
	}else{
		res.status(201).send(json);
	}
});

module.exports = app.listen(port);
console.log('Server is listening on port '+port);
