# Backend API

## Installation
The containerized project is available at `registry.gitlab.com/jgavin0222/node-backend-api`  
```
docker pull registry.gitlab.com/jgavin0222/node-backend-api
docker run -dit -p 4500:4500 --name node-backend-api registry.gitlab.com/jgavin0222/node-backend-api
```

## Removal
To delete the containerized project use the following:
```
docker container stop node-backend-api
docker container rm node-backend-api
docker image rm registry.gitlab.com/jgavin0222/node-backend-api
```
